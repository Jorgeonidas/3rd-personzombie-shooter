﻿using UnityEngine;
using System.Collections;

public class DoorUnlockerScript : MonoBehaviour {
	public Material unlockMat;
	public GameObject lockPanel;
	public GameObject frameBubble;
	public GameObject frameBubble1;
	//GameObject baseGo;
	public GameObject baseGo;
	DoorScript Ds;
	AudioSource audioSource;

	void Start(){
		Ds = baseGo.GetComponent<DoorScript> ();
		audioSource = GetComponent<AudioSource> ();
		if (Ds.locked == false) {
			frameBubble.GetComponent<Renderer> ().material = unlockMat;
			frameBubble1.GetComponent<Renderer> ().material = unlockMat;
		}
	}

	void OnTriggerEnter(Collider other){
		//Debug.Log ("al alcance de el locker");
		if (other.tag == "Player") {
				Debug.Log ("uncloking");
				//lockPanel.GetComponent<Renderer> ().material = unlockMat;
			}
	
	}

	void OnTriggerStay(Collider other){
		if (other.tag == "Player") {
			if(Input.GetKeyDown(KeyCode.E)){
				lockPanel.GetComponent<Renderer> ().material = unlockMat;
				frameBubble.GetComponent<Renderer> ().material = unlockMat;
				frameBubble1.GetComponent<Renderer> ().material = unlockMat;
				//GetComponentInParent<DoorScript>().lockUnlockDorr();
				Ds.lockUnlockDorr();
				audioSource.Play();
			}
		}
	}
}
