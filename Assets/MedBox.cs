﻿using UnityEngine;
using System.Collections;
using UnitySampleAssets.Characters.ThirdPerson;

public class MedBox : MonoBehaviour {
	public GameObject pickItem;
	PickableItem pickableItem;
	// Use this for initialization
	void Start () {
		pickableItem = pickItem.GetComponent<PickableItem> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(pickableItem.characterInTrigger)//si esta dentro del trigger
		{
			if(pickableItem.owner)
			{
				if(pickableItem.owner.GetComponent<ThirdPersonUserControl>())//es el jugador?
				{
					pickableItem.owner.GetComponent<ThirdPersonUserControl>().canPickUp = true;
					pickableItem.owner.GetComponent<ThirdPersonUserControl>().item = this.gameObject;
				}
			}
		}
		else//si no desasignar el dueño
		{
			if(pickableItem.owner)
			{
				if(pickableItem.owner.GetComponent<ThirdPersonUserControl>())//es el jugador?
				{
					pickableItem.owner.GetComponent<ThirdPersonUserControl>().canPickUp = false;
					pickableItem.owner.GetComponent<ThirdPersonUserControl>().item = null;
					pickableItem.owner = null;
				}
			}
		}
	}

}

