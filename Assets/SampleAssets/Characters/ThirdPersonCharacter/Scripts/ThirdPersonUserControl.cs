using UnityEngine;
using UnitySampleAssets.CrossPlatformInput;
using System.Collections;
using UnitySampleAssets.Cameras;
using UnityEngine.UI;

namespace UnitySampleAssets.Characters.ThirdPerson
{

    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class ThirdPersonUserControl : MonoBehaviour
    {

        public bool walkByDefault = false; // toggle for walking state

        public bool lookInCameraDirection = true;// should the character be looking in the same direction that the camera is facing

        private Vector3 lookPos; // The position that the character should be looking towards
        private ThirdPersonCharacter character; // A reference to the ThirdPersonCharacter on the object
        private Transform cam; // A reference to the main camera in the scenes transform
        private Vector3 camForward; // The current forward direction of the camera

        private Vector3 move;
        private bool jump;// the world-relative desired move direction, calculated from the camForward and user input.
	
		//JORGE
		public Camera camarita;
		public Transform pivote;
		public bool aim;
		float aimingWeight;
		EnemyHealth enemyHealth;
		Animator anim;

		WeaponManager weaponManager;
		float timer;

		FreeLookCam camFunction;//funciones de la camara
		//pickables
		public bool canPickUp;
		public GameObject item;
		public GameObject pickText;
		//UI
		public Text curAmmo;
		public Text carryingAmmo;
		public GameObject VictoryPanel;
		//public Slider HealthBarr;

        // Use this for initialization
        private void Start()
        {
			//isAiming = false;//JORGE
            // get the transform of the main camera
            if (Camera.main != null)
            {
                cam = Camera.main.transform;
				//Camera.main.transform.root.GetComponent<F>
            }
            else
            {
                Debug.LogWarning(
                    "Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.");
                // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
            }
			anim = GetComponent<Animator>();
            // get the third person character ( this should never be null due to require component )
            character = GetComponent<ThirdPersonCharacter>();
			weaponManager = GetComponent<WeaponManager> ();
			//JORGE
			camFunction = Camera.main.transform.root.GetComponent<FreeLookCam> ();
        }

        void Update()
        {
			timer += Time.deltaTime;
			aim = Input.GetMouseButton (1);
            if(!jump)
                jump = CrossPlatformInputManager.GetButtonDown("Jump");

			//disparar!!!
			if (aim) {
				bool canFire = ShareFunctions.CheckAmmo(weaponManager.ActiveWeapon);

				if(weaponManager.ActiveWeapon.CanBurst)
				{
					if(Input.GetMouseButtonDown(0) && !anim.GetCurrentAnimatorStateInfo(2).IsTag("Reload"))
					{
						if(canFire){
							timer = 0;
							anim.SetTrigger("Fire");
							ShootRay();
							weaponManager.ActiveWeapon.GetComponent<AudioSource>().Play();
							camFunction.WiggleCrosshairAndCamera(weaponManager.ActiveWeapon,true);
							weaponManager.ActiveWeapon.curAmmo--;
						}
						else
						{
							weaponManager.ReloadActiveWeapomn();
							anim.SetTrigger("Reload");
						}
					}
				}
				else
				{
					if(Input.GetMouseButton(0) && !anim.GetCurrentAnimatorStateInfo(2).IsTag("Reload"))
					{
						if(canFire)
						{
							if(timer >= weaponManager.ActiveWeapon.firerate){//simular FireRate
								timer = 0;
								anim.SetTrigger("Fire");
								ShootRay();
								camFunction.WiggleCrosshairAndCamera(weaponManager.ActiveWeapon,true);
								weaponManager.ActiveWeapon.curAmmo--;
								weaponManager.ActiveWeapon.GetComponent<AudioSource>().Play();
							}
						}
						else
						{
							weaponManager.ReloadActiveWeapomn();
							anim.SetTrigger("Reload");
						}
					}
				}
			}
			if (Input.GetAxis ("Mouse ScrollWheel") != 0) 
			{
				if(Input.GetAxis("Mouse ScrollWheel") < -0.1)
				{
					weaponManager.ChangeWeapon(false);
				}
				if(Input.GetAxis("Mouse ScrollWheel") > 0.1)
				{
					weaponManager.ChangeWeapon(true);
				}
			}
			pickUpItem ();
			updateUI ();
        }

		void updateUI()
		{
			curAmmo.text = weaponManager.ActiveWeapon.curAmmo.ToString();
			carryingAmmo.text = weaponManager.ActiveWeapon.currentCarryingAmmo.ToString();
			//HealthBarr.value = GetComponent<CharacterStats> ().health;
		}

		public void pickUpItem(){
			if (canPickUp) {
				if (!pickText.activeInHierarchy) {
					pickText.SetActive (true);

					pickText.GetComponent<Text>().text = "presiona 'E' para tomar: " + item.name;
				}
				if (Input.GetKeyUp (KeyCode.E)) 
				{
					pickText.SetActive (false);
					ShareFunctions.PickUpItem (this.gameObject, item);//funcion compartida para coger objetos
					canPickUp = false;
				}
			} 
			else 
			{
				if (pickText.activeInHierarchy) {
					pickText.SetActive (false);
				}
			}
		}
		//Prefab para la bala a adisparar
		public GameObject bulletPrefab;
		//disparar hacia el centro de la camara
		void ShootRay()
		{
			//mitad de la pantalla
			float x = Screen.width / 2;
			float y = Screen.height / 2;

			Ray ray = Camera.main.ScreenPointToRay (new Vector3 (x,y,0));

			RaycastHit hit;//captura el tipo de objeto al que el rayo (Ray) atina

			GameObject go = Instantiate(bulletPrefab,transform.position, Quaternion.identity) as GameObject;

			LineRenderer line = go.GetComponent<LineRenderer>();//renderizar lineas en el modo editor

			Vector3 startPos = weaponManager.ActiveWeapon.bulletSpawn.TransformPoint(Vector3.zero);//local a global
			Vector3 endPos = Vector3.zero;

			int mask = ~(1 << 10);//ignora la capa numero 10 "Destruibles"

			if (Physics.Raycast (ray, out hit, Mathf.Infinity, mask)) { //si le di a algo con el rayo ( Ray ray )
				float distance = Vector3.Distance (weaponManager.ActiveWeapon.bulletSpawn.position, hit.point);//distancia de el cañon del arma al hitpoin

				RaycastHit[] hits = Physics.RaycastAll (startPos, hit.point - startPos, distance);//todos los rayos desde el personaje hasta el hitpoint en el medio de la pantalla

				foreach (RaycastHit hit2 in hits) {
					Debug.Log(hit2.collider.gameObject.tag);//saber exactamente a que gameObject le doy ( si es un hijo de otro go)
					if (hit2.transform.GetComponent<Rigidbody> ()) {//si le dimos a un cuerpo rigido
						if(hit2.transform.GetComponent<EnemyHealth>()){//si es un zombie
							enemyHealth = hit2.transform.GetComponent<EnemyHealth>();
							enemyHealth.damageTaken(weaponManager.ActiveWeapon.weaponDamage);//le hago daño
							Debug.Log(enemyHealth.currHealth);
						}
						Vector3 direction = hit2.transform.position - transform.position;
						direction = direction.normalized;
						hit2.transform.GetComponent<Rigidbody> ().AddForce (direction * 200);
					}
					else if(hit2.transform.GetComponent<DesctructibleWall>()) 
					{	
						hit2.transform.GetComponent<DesctructibleWall>().destruct = true;
						//digamos que es el server
						VictoryPanel.SetActive(true);

					}
					//destruir Stwiches
					if (hit2.collider.gameObject.tag == "PowerBox"){
						Destroy(hit2.collider.gameObject);
					}
				}

				endPos = hit.point;

			} else {//si el rayo fue al infinito sin atinarle a nada
				endPos = ray.GetPoint(100);//un punto a los 100 metros
			}
			line.SetPosition (0, startPos);
			line.SetPosition (1, endPos);
		}


        // Fixed update is called in sync with physics
        private void FixedUpdate()
        {
            // read inputs
            bool crouch = false;

            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            float v = CrossPlatformInputManager.GetAxis("Vertical");
            crouch = Input.GetKey(KeyCode.C);

			//JORGE
			Vector3 pivotePos = pivote.transform.localPosition;

			if (crouch) {
				pivotePos.y = 1f;
				pivote.transform.localPosition = pivotePos;
			} else {
				pivotePos.y = 1.5f;
				pivote.transform.localPosition = pivotePos;
			}
			//JORGE

            // calculate move direction to pass to character
            if (cam != null)
            {
                // calculate camera relative direction to move:
                camForward = Vector3.Scale(cam.forward, new Vector3(1, 0, 1)).normalized;
                move = v*camForward + h*cam.right;
            }
            else
            {
                // we use world-relative directions in the case of no main camera
                move = v*Vector3.forward + h*Vector3.right;
            }

            if (move.magnitude > 1) move.Normalize();

#if !MOBILE_INPUT
            // On non-mobile builds, walk/run speed is modified by a key press.
            bool walkToggle = Input.GetKey(KeyCode.LeftShift);
            // We select appropriate speed based on whether we're walking by default, and whether the walk/run toggle button is pressed:
            float walkMultiplier = (walkByDefault ? walkToggle ? 1 : 0.5f : walkToggle ? 0.5f : 1);
            move *= walkMultiplier;
#endif

            // calculate the head look target position
            lookPos = lookInCameraDirection && cam != null
                          ? transform.position + cam.forward*100
                          : transform.position + transform.forward*100;

            // pass all parameters to the character control script
            character.Move(move, crouch, jump, lookPos);
            jump = false;


			//JORGE Apuntar

			if(aim){
				move = Vector3.zero;

				Vector3 dir = lookPos - transform.position;
				dir.y = 0;
				//JORGE rotar el personaje a la direccion de la camara
				transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(dir),20*Time.deltaTime);

				if(!crouch)
					walkByDefault = true;
				else {
					walkByDefault = false;
				}
				//cambia estado
				anim.SetBool("Aim",aim);

				anim.SetFloat("Forward", v);
				anim.SetFloat("Turn", h);
				//

				//aca rotacion

				//
				//Debug.Log(spine.rotation);
				//zoom a la camara
				lookInCameraDirection = true;
				camarita.fieldOfView = 30;//JORGE reemplazar por nueva posicion en el pivote
			}else{
				walkByDefault = false;
				lookInCameraDirection = false;
				camarita.fieldOfView = 60;//JORGE reemplazar por nueva posicion en el pivote
				anim.SetBool("Aim",aim);
			}
			//JORGE

        }


    }
}


/**/