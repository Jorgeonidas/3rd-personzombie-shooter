﻿using UnityEngine;
using System.Collections;

public class AtackRange : MonoBehaviour {
	public bool inAtackRange;

	// Use this for initialization
	void OnTriggerEnter(Collider other){
		if (other.GetComponent<CharacterStats>())
			inAtackRange = true;
	}
	void OnTriggerExit(Collider other){
		if (other.GetComponent<CharacterStats> ())
			inAtackRange = false;
	}

}
