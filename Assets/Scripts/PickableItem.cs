﻿using UnityEngine;
using System.Collections;

public class PickableItem : MonoBehaviour {
	
	public bool characterInTrigger;
	public GameObject owner;

	void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<CharacterStats>()) 
		{
			characterInTrigger = true;

			if(!owner)
			{
				owner = other.gameObject;
			}
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.GetComponent<CharacterStats> ()) 
		{
			if(owner != null)
			{
				if(other.GetComponent<CharacterStats> ().gameObject == owner)
				{
					characterInTrigger = false;
				}
			}
		}
	}
}
