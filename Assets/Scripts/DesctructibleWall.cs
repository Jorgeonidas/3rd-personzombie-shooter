﻿using UnityEngine;
using System.Collections;

public class DesctructibleWall : MonoBehaviour {
	public bool destruct;
	public Transform[] debris;
	// Use this for initialization
	void Start () 
	{
		foreach (Transform go in debris) 
		{
			go.gameObject.SetActive(false);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (destruct) {
			foreach(Transform go in debris){
				go.gameObject.SetActive(true);
				go.transform.parent = null;
			}
			Destroy(gameObject);
		}

	}
}
