﻿using UnityEngine;
using System.Collections;

public class DoorScript : MonoBehaviour {

	public GameObject door;
	public bool locked;

	AudioSource audioSource;
	
	void Start(){
		audioSource = GetComponent<AudioSource> ();
	}

	void OnTriggerEnter(Collider other){
		if (other.tag == "Player" && !locked) {
			door.GetComponent<Animation> ().Play ("open");
			audioSource.Play ();
		}
	}

	void OnTriggerExit(Collider other){
		if(other.tag== "Player" && !locked)
			door.GetComponent<Animation> ().Play ("close");	
	}

	public void lockUnlockDorr(){
		if (locked == true) {
			locked = false;
		}
	}
}
