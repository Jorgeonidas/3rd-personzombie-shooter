﻿using UnityEngine;
using System.Collections;

public class ItemID : MonoBehaviour {

	public enum ItemType{
		Weapons,
		Health,
		Ammo,
		Wearable
	}

	public ItemType itemType;
}
