﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour
{
    Transform player;
    //PlayerHealth playerHealth;
    EnemyHealth enemyHealth;
    UnityEngine.AI.NavMeshAgent nav;
	public GameObject eyes;
	EnemySight sight;

	public float detectDistance = 10f;
	[SerializeField]
	private float distance;
	[SerializeField]
	private bool detected;
	Animator anim;
	AudioSource audioSoruce;


    void Awake ()
    {
        player = GameObject.FindGameObjectWithTag ("Player").transform;
        //playerHealth = player.GetComponent <PlayerHealth> ();
        enemyHealth = GetComponent <EnemyHealth> ();
        nav = GetComponentInChildren <UnityEngine.AI.NavMeshAgent> ();
		anim = GetComponent<Animator> ();

		sight = eyes.GetComponent<EnemySight>();
		audioSoruce = GetComponent<AudioSource> ();
    }


    void Update ()
    {	//el agente de navegacion estara activo si enemigo y player estan vivo
		if(sight.canSeePlayer  && GetComponent<EnemyHealth>().currHealth > 0 && !GetComponentInChildren<AtackRange>().inAtackRange )
        {
			anim.SetBool("walk", true);
			nav.enabled = true;
			audioSoruce.Play();
            nav.SetDestination (player.position);

        }
        	else
        {
			anim.SetBool("walk", false);
            nav.enabled = false;
        }

    }
	
}
