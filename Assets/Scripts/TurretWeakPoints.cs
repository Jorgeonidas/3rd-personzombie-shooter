﻿using UnityEngine;
using System.Collections;

public class TurretWeakPoints : MonoBehaviour {
	BoxCollider[] weakpoints;
	// Use this for initialization
	void Start () {
		weakpoints = GetComponentsInChildren<BoxCollider> ();

	}
	
	// Update is called once per frame
	void Update () {

		//la torreta tiene 2 hijos por defecto que es el modelo y el cañon los demas serian sus puntos debiles
		if (transform.childCount <= 2) {
			GetComponentInChildren<TurretRotation>().enabled = false;
		}
	}
}
