﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CharacterStats : MonoBehaviour {

	public Slider HealthBarr;
	public string id;
	public float health;
	//UI
	public GameObject DeadPanel;
	void Start(){
		DeadPanel.SetActive (false);
	}

	public void takeDamage(int dmg){
		health -= dmg;
		HealthBarr.value = health;
		if (health <= 0) {
			DeadPanel.SetActive(true);
		}
	}
	public void heal(int ammount){
		health += ammount;
		HealthBarr.value = health;
	}
}
