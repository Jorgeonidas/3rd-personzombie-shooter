﻿using UnityEngine;
using System.Collections;

public class ParticleDirectionScript : MonoBehaviour {

	public Transform weapon;//posicion del arma

	// Update is called once per frame
	void Update () {
		transform.position = weapon.TransformPoint (Vector3.zero);
		transform.forward = weapon.TransformDirection (Vector3.forward);
	}
	//colision de particula con un objeto
	void OnParticleCollision(GameObject other)
	{
		Rigidbody otherRgb = other.GetComponent<Rigidbody> ();
		if (otherRgb) {
			Vector3 direction = otherRgb.transform.position - transform.position;
			direction = direction.normalized;

			otherRgb.AddForce(direction*50);
		}
	}
}
