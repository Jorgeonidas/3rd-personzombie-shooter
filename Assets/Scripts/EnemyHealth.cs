﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour {
	public int maxHealth;
	public int currHealth;
	Animator anim;
	UnityEngine.AI.NavMeshAgent nav;
	CapsuleCollider cap;
	public GameObject atackRange;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		currHealth = maxHealth;
		nav = GetComponentInChildren<UnityEngine.AI.NavMeshAgent> ();
		cap = GetComponent<CapsuleCollider> ();
	}

	//functions
	public void damageTaken(int dmg){
		currHealth -= dmg;
		if (currHealth <= 0) {
			die();
		}
	}
	public void die(){
		anim.SetTrigger ("Dead");
		cap.enabled = false;
		atackRange.GetComponent<SphereCollider>().enabled =false;
		Destroy (gameObject, 2f);
		//anim.enabled = false;
		//GetComponent<>
	}
}
