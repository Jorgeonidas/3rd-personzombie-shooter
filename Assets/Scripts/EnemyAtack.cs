﻿using UnityEngine;
using System.Collections;
using UnitySampleAssets.Characters.ThirdPerson;

public class EnemyAtack : MonoBehaviour {
	public int damage;
	Transform player;
	CharacterStats playerStats;
	public float timeBetweenAtacks = 1f;
	Animator anim;
	Animator playerAnim;
	float timer;
	void Start () {
		anim = GetComponent<Animator> ();
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		playerStats = player.GetComponent<CharacterStats> ();
		playerAnim = player.gameObject.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if (GetComponentInChildren<AtackRange>().inAtackRange && timer >= timeBetweenAtacks && playerStats.health > 0) {
			timer = 0;
			anim.SetTrigger("Atack");
			atack();
			Debug.Log(playerStats.health);
		}
	}
	public void atack(){
		//resto el daño
		playerStats.takeDamage(damage);
		if(playerStats.health <= 0){
			player.gameObject.GetComponent<ThirdPersonUserControl>().enabled = false;
			//player.gameObject.GetComponent<Rigidbody>().useGravity = true;
			player.gameObject.GetComponent<CapsuleCollider>().enabled = false;
			playerAnim.SetTrigger("Death");
			Vector3 deathPos = player.transform.position;
			deathPos.y -= 0.85f;
			player.transform.position = deathPos;
		}
	}

}
