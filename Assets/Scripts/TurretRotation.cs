﻿using UnityEngine;
using System.Collections;

public class TurretRotation : MonoBehaviour {
	Transform player;
	public float turnSpeed = 5f;
	Vector3 direction;
	EnemySight turretSight;
	float timer;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		turretSight = GetComponent<EnemySight> ();
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if (turretSight.canSeePlayer && player.GetComponent<CharacterStats>().health > 0) {
			direction = transform.position - player.position;
			direction.Normalize();
			Quaternion targetRotation = Quaternion.LookRotation(-direction);
			targetRotation.z = 0;
			targetRotation.x = 0;
			transform.rotation = Quaternion.Slerp(transform.rotation,targetRotation ,turnSpeed*Time.deltaTime);
			if(timer >= GetComponent<EnemyShooting>().fireRate){
				timer = 0;
				GetComponent<EnemyShooting>().Shootray();
			}
		}
	}
}
