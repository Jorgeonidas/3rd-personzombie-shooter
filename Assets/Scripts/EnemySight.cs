﻿using UnityEngine;
using System.Collections;

public class EnemySight : MonoBehaviour {

	Transform player;
	public float fieldOfViewRange;
	public float minPlayerDetectDistance;
	public float rayRange;
	Vector3 rayDirection;
	public bool canSeePlayer;

	// Use this for initialization
	void Start () {
		rayDirection = Vector3.zero;
		player = GameObject.FindGameObjectWithTag ("Player").transform;
	}
	void Update(){
		CanSeePlayer ();
	}

	public void CanSeePlayer(){
	
		RaycastHit hit;
		rayDirection = player.position - transform.position;
		float distanceToPlayer = Vector3.Distance (transform.position, player.position);
		if(Physics.Raycast (transform.position, rayDirection,out hit)){ // If the player is very close behind the enemy and not in view the enemy will detect the player
			if((hit.transform.tag == "Player") && (distanceToPlayer <= minPlayerDetectDistance)){
				//Debug.Log("Caught player sneaking up behind!");
				canSeePlayer = true;
			}else{
				canSeePlayer = false;
			}
		}
		if((Vector3.Angle(rayDirection, transform.forward)) <= fieldOfViewRange){ // Detect if player is within the field of view
			if (Physics.Raycast (transform.position, rayDirection,out hit, rayRange)) {
				if (hit.transform.tag == "Player") {
					//Debug.Log("Can see player");
					canSeePlayer = true;
				}else{
					//Debug.Log("Can not see player");
					canSeePlayer = false;
				}
			}
		}
		
	}

	
	void OnDrawGizmosSelected ()
	{
		float halfFOV = fieldOfViewRange;
		// Draws a line in front of the player and one behind this is used to visually illustrate the detection ranges in front and behind the enemy
		Gizmos.color = Color.magenta; // the color used to detect the player in front
		//Gizmos.DrawRay (transform.position, transform.forward * rayRange);
		Quaternion leftRayRotation;
		Quaternion rightRayRotation;
		
		leftRayRotation = Quaternion.AngleAxis( -halfFOV, Vector3.up );
		rightRayRotation = Quaternion.AngleAxis( halfFOV, Vector3.up );
		
		Vector3 leftRayDirection;
		Vector3 rightRayDirection;
		leftRayDirection = leftRayRotation * transform.forward;
		rightRayDirection = rightRayRotation * transform.forward;
		
		Gizmos.DrawRay( transform.position, leftRayDirection * rayRange );
		Gizmos.DrawRay( transform.position, rightRayDirection * rayRange );
		
		Gizmos.color = Color.yellow; // the color used to detect the player from behind
		Gizmos.DrawRay (transform.position, transform.forward * minPlayerDetectDistance);
		Gizmos.DrawRay (transform.position, -transform.forward * minPlayerDetectDistance);
		Gizmos.DrawRay (transform.position, transform.right * minPlayerDetectDistance);
		Gizmos.DrawRay (transform.position, -transform.right * minPlayerDetectDistance);

	}
}
