﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class CrossHair : MonoBehaviour {

	public string CrossHairName;

	public float defaultSpread = 15;
	public float maxSpread = 50;
	public float wiggleSpread = 50;
	public float wiggleSpreadTimmer = 50;

	[HideInInspector]
	public float currentSpread = 0;
	private float targetSpread = 0;
	private float SpreadTimer = 0;
	private Quaternion defaultRotation;
	private bool isSpreadWorking = true;

	public float spreadSpeed = 0.2f;
	public float rotationSpeed = 0.5f;
	public bool allowRotation = true;

	public float rotationTimer = 0;
	private bool isRotagin= false;

	public bool spreadWhileRotating = false;
	public float rotationSpread = 0;
	public bool allowSpread = true;

	private bool wiggle = false;
	private float wiggleTimer = 0;

	[Serializable]
	public class CrossHairPart
	{
		public Image image;
		public Vector2 up;
	}
	public CrossHairPart[] parts;

	void Start()
	{
		defaultRotation = transform.rotation;
		currentSpread = defaultSpread;

		ChangeCursorSpread (defaultSpread);
	}

	void Update()
	{
		if (isSpreadWorking) 
		{
			SpreadTimer += Time.deltaTime / spreadSpeed;

			float spreadValue = AccelDecelInterpolation(currentSpread, targetSpread, SpreadTimer);

			if(SpreadTimer >1)
			{
				spreadValue = targetSpread;
				SpreadTimer = 0;

				if(wiggle)
				{
					if(wiggleTimer < wiggleSpreadTimmer)
					{
						wiggleTimer += Time.deltaTime;
					}
					else
					{
						wiggleTimer = 0;
						wiggle =false;
						targetSpread = defaultSpread;
					}
				}
				else
				{
					isSpreadWorking = false;
				}

			}
			else//if(SpreadTimer >1)
			{
				ChangeCursorSpread(defaultSpread);	
			}
			currentSpread = spreadValue;
			ApplySpread();
		}//if (isSpreadWorking) 

		if(isRotagin)
		{
			if(rotationTimer >0)
			{
				rotationTimer -= Time.deltaTime;
				transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, 
				                                      transform.rotation.eulerAngles.y,
				                                      transform.rotation.eulerAngles.z + (360 * Time.deltaTime * rotationSpeed) );
			}
			else//if(rotationTimer >0)
			{
				isRotagin = false;
				transform.rotation = defaultRotation;

				if(spreadWhileRotating)
				{
					ChangeCursorSpread(defaultSpread);
				}
			}
		}
	}
	
	public void ApplySpread()
	{
		foreach(CrossHairPart im in parts)
		{
			im.image.rectTransform.anchoredPosition = im.up * currentSpread;

		}
	}

	public void WiggleCrosshair()
	{
		if (allowSpread) 
		{
			ChangeCursorSpread(wiggleSpread);
			wiggle = true;
		}
	}

	public void ChangeCursorSpread(float value)
	{
		if (allowSpread) 
		{
			isSpreadWorking = true;
			targetSpread = value;
			SpreadTimer = 0;
		}
	}

	public void rotateCursor(float seconds)
	{
		if (allowRotation) 
		{
			isRotagin = true;
			rotationTimer = seconds;

			if(spreadWhileRotating)
			{
				ChangeCursorSpread(rotationSpeed);
			}
		}
	}

	public static float AccelDecelInterpolation(float start, float end, float t)
	{
		float x = end - start;

		float newT = (Mathf.Cos ((t + 1) * Mathf.PI) / 2) + 0.5f;

		x *= newT;

		float retValue = start + x;

		return retValue;
	}




}
