﻿using UnityEngine;
using System.Collections;
using UnitySampleAssets.Characters.ThirdPerson;

public class EnemyShooting : MonoBehaviour {
	public Transform GunBarrelPos;
	public GameObject BulletGo;
	LineRenderer bulletLine;
	public float fireRate;
	public int damage;
	AudioSource audioSource;

	void Start(){
		audioSource = GetComponent<AudioSource> ();
	}

	public void Shootray(){

		GameObject go = Instantiate(BulletGo,transform.position, Quaternion.identity) as GameObject;

		bulletLine = go.GetComponent<LineRenderer> ();

		Vector3 startPos = GunBarrelPos.position;
		Vector3 endPos = Vector3.zero;
		Ray ray = new Ray(transform.position, transform.forward);
		RaycastHit hit;//captura el tipo de objeto al que el rayo (Ray) atina
		int mask = ~(1 << 10);//ignora la capa numero 10 "Destruibles"

		if (Physics.Raycast (ray, out hit, Mathf.Infinity, mask)) { //si le di a algo con el rayo ( Ray ray )
			float distance = Vector3.Distance (startPos, hit.point);//distancia de el cañon del arma al hitpoin
			
			RaycastHit[] hits = Physics.RaycastAll (startPos, hit.point - startPos, distance);//todos los rayos desde el personaje hasta el hitpoint en el medio de la pantalla
			
			foreach (RaycastHit hit2 in hits) {
				if (hit2.transform.GetComponent<Rigidbody> ()) {//si le dimos a un cuerpo rigido
					if(hit2.transform.GetComponent<CharacterStats>()){//si es un zombie
						hit2.transform.GetComponent<CharacterStats>().takeDamage(damage);
						//mata al player (mejorar animacion)
						if(hit2.transform.GetComponent<CharacterStats>().health <= 0){
							hit2.transform.GetComponent<Animator>().SetTrigger("Death");
							hit2.transform.GetComponent<ThirdPersonUserControl>().enabled = false;
							hit2.transform.GetComponent<CapsuleCollider>().isTrigger = true;
							Vector3 deathPos = hit2.transform.GetComponent<Transform>().position;
							deathPos.y -= 0.3f;
							hit2.transform.position = deathPos;
						}
						//enemyHealth.damageTaken(weaponManager.ActiveWeapon.weaponDamage);//le hago daño
						Debug.Log(hit2.transform.GetComponent<CharacterStats>().health);
					}
					Vector3 direction = hit2.transform.position - transform.position;
					direction = direction.normalized;
					hit2.transform.GetComponent<Rigidbody> ().AddForce (direction * 200);
				}
				else if(hit2.transform.GetComponent<DesctructibleWall>()) 
				{	
					hit2.transform.GetComponent<DesctructibleWall>().destruct = true;
				}
			}
			
			endPos = hit.point;
			
		} else {//si el rayo fue al infinito sin atinarle a nada
			endPos = ray.GetPoint(100);//un punto a los 100 metros
		}
		audioSource.Play ();
		bulletLine.SetPosition (0, startPos);
		bulletLine.SetPosition (1, endPos);
	}
}
