﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponManager : MonoBehaviour {

	public List<GameObject> WeaponList = new List<GameObject>(); // lista de armas
	public WeaponControler ActiveWeapon; //arma seleccionada actualmente

	int weaponNumber = 0;
	public int maxCarryingWeapons = 2;

	public enum WeaponType
	{
		Pistol,
		Rifle
	}

	public WeaponType weaponType;

	Animator anim;

	[System.Serializable] public class IKTargetPos
	{
		[Header("Targets")]
		public Transform HandPlacement;
		public Transform ElbowPlacement;

		[Header("Elbow Positions")]
		public Vector3 elbowPistolPos = new Vector3(-2.30f, 0.9f, 2.78f);
		public Vector3 elbowRiflePos = new Vector3(-2.30f, 0.9f, 2.78f);

		public bool debugIK;
	}

	public IKTargetPos ikTargetPos;

	// Use this for initialization
	void Start () 
	{
		ActiveWeapon = WeaponList [weaponNumber].GetComponent<WeaponControler>();
		//ikTargetPos.HandPlacement = ActiveWeapon.HandPosition.transform;
		//ikTargetPos.ElbowPlacement = new GameObject ().transform;
		ActiveWeapon.equip = true;

		anim = GetComponent<Animator> ();

		foreach (GameObject go in WeaponList) {
			go.GetComponent<WeaponControler>().hasOwner = true;
		}
	}
	
	// Update is called once per frame
	void Update () {
		ActiveWeapon = WeaponList [weaponNumber].GetComponent<WeaponControler>();
		ActiveWeapon.equip = true;
		weaponType = ActiveWeapon.weaponType;

		if (!ikTargetPos.debugIK) {
			switch(weaponType)
			{
				case WeaponType.Pistol:	
					anim.SetInteger("Weapon",0);
					//ikTargetPos.ElbowPlacement.localPosition = ikTargetPos.elbowPistolPos;
					break;
				case WeaponType.Rifle:
					anim.SetInteger("Weapon", 1);
					//ikTargetPos.ElbowPlacement.localPosition = ikTargetPos.elbowRiflePos;
					break;
			}
		}
	}

	//funciones

	public void FireActiveWeapon()
	{
		if (ActiveWeapon != null) 
		{
			ActiveWeapon.Fire();
		}
	}

	public void ChangeWeapon(bool ascending)
	{
		if (WeaponList.Count > 1) 
		{
			ActiveWeapon.equip = false;

			if(ascending){
				if( weaponNumber <  WeaponList.Count - 1){
					weaponNumber ++;
				}
				else
				{
					weaponNumber = 0;
				}
			}
			else
			{
				if(weaponNumber > 0)
				{
					weaponNumber-- ;
				}
				else
				{
					weaponNumber = WeaponList.Count -1;
				}
			}
		}
	}

	public void ReloadActiveWeapomn ()
	{
		int cur = ActiveWeapon.maxCarryingAmmo - (ActiveWeapon.maxCarryingAmmo - ActiveWeapon.currentCarryingAmmo);

		if (cur > 0) {
			if (cur > ActiveWeapon.MaxClipAmmo) {
				ActiveWeapon.curAmmo = ActiveWeapon.MaxClipAmmo;
				ActiveWeapon.currentCarryingAmmo -= ActiveWeapon.MaxClipAmmo;
			} else {
				ActiveWeapon.curAmmo = cur;
				ActiveWeapon.currentCarryingAmmo -= cur;
			}
		}
	}

}
