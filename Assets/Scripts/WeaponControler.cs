﻿using UnityEngine;
using System.Collections;
using UnitySampleAssets.Characters.ThirdPerson;

[RequireComponent(typeof(ItemID))]
[RequireComponent(typeof(Rigidbody))]
public class WeaponControler : MonoBehaviour {
	public bool equip;
	public WeaponManager.WeaponType weaponType;

	public int currentCarryingAmmo;
	public int maxCarryingAmmo = 50;

	public int MaxAmmo;
	public int MaxClipAmmo = 30;
	public int curAmmo;
	public bool CanBurst;
	public float kickBack = 0.1f;
	public int weaponDamage = 20;
	public float firerate;
	//public GameObject HandPosition;
	public Transform bulletSpawn;
	public GameObject bulletPrefab;
	GameObject bulletSpawnGO;
	ParticleSystem bulletParticles;

	WeaponManager parentControl;
	bool fireBullet;

	AudioSource audioSource;

	Animator weaponAnim;

	Rigidbody rigidBody;
	BoxCollider boxCol;
	SphereCollider sphereCol;
	PickableItem pickableItem;
	public GameObject pickableGO;

	[Header("Positions")]//titulo de las variables en el inspector
	public bool hasOwner;
	public Vector3 equipPosition;
	public Vector3 equipRotation;
	public Vector3 UnEquiPosition;
	public Vector3 UnEquiRotation;
	//debugScale
	Vector3 scale;

	public enum RestPosition
	{
		RightHip,
		Waist
	}
	public RestPosition restPosition;
	// Use this for initialization
	void Start () {

		currentCarryingAmmo = maxCarryingAmmo;

		curAmmo = MaxClipAmmo;
		/*bulletSpawnGO = Instantiate (bulletPrefab, transform.position, Quaternion.identity) as GameObject; //la posicion donde saldran las balas
		bulletSpawnGO.GetComponent<ParticleDirectionScript>();
		bulletSpawnGO.GetComponent<ParticleDirectionScript>().weapon = bulletSpawn;
		bulletParticles = bulletSpawnGO.GetComponent<ParticleSystem>();*/
	
		rigidBody = GetComponent<Rigidbody> ();
		rigidBody.isKinematic = true;
		boxCol = GetComponent<BoxCollider> ();
		pickableItem = GetComponentInChildren<PickableItem> ();
		sphereCol = GetComponentInChildren<SphereCollider> ();

		audioSource = GetComponent<AudioSource> ();
		weaponAnim = GetComponent<Animator> ();
		scale = transform.localScale;
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.localScale = scale;
		if (equip) {
			//el arma sera hija del animator del personaje en su mano derecha
			transform.parent = transform.GetComponentInParent<WeaponManager> ().transform.GetComponent<Animator> ().GetBoneTransform (HumanBodyBones.RightHand);
			transform.localPosition = equipPosition;
			transform.localRotation = Quaternion.Euler (equipRotation);

			if(pickableItem.gameObject.activeInHierarchy)
			{
				pickableItem.gameObject.SetActive(false);
			}

		} 
		else 
		{
			if(hasOwner)
			{
				if(pickableItem.gameObject.activeInHierarchy)
				{
					pickableItem.gameObject.SetActive(false);
				}
				//tengo dueño
				boxCol.isTrigger = true;
				rigidBody.isKinematic = true;

				switch(restPosition)
				{
					//posicion del modelo en donde tendra "Equipada" el arma
					case RestPosition.RightHip:
						transform.parent = transform.GetComponentInParent<WeaponManager> ().transform.GetComponent<Animator> ().GetBoneTransform (HumanBodyBones.RightUpperLeg);
						break;
					case RestPosition.Waist:
						transform.parent = transform.GetComponentInParent<WeaponManager> ().transform.GetComponent<Animator> ().GetBoneTransform (HumanBodyBones.Spine);
						break;
				}
				transform.localPosition = UnEquiPosition;
				transform.localRotation = Quaternion.Euler(UnEquiRotation);
			}
			else//if(hasOwner)
			{
				if(!pickableItem.gameObject.activeInHierarchy)
				{
					pickableItem.gameObject.SetActive(true);
				}
				//no tengo dueño
				boxCol.isTrigger = false;
				rigidBody.isKinematic = false;

				if(pickableItem.characterInTrigger)
				{
					if(pickableItem.owner)
					{
						if(pickableItem.owner.GetComponent<ThirdPersonUserControl>())//es el jugador?
						{
							pickableItem.owner.GetComponent<ThirdPersonUserControl>().canPickUp = true;
							pickableItem.owner.GetComponent<ThirdPersonUserControl>().item = this.gameObject;
						}
					}
				}
				else
				{
					if(pickableItem.owner)
					{
						if(pickableItem.owner.GetComponent<ThirdPersonUserControl>())//es el jugador?
						{
							pickableItem.owner.GetComponent<ThirdPersonUserControl>().canPickUp = false;
							pickableItem.owner.GetComponent<ThirdPersonUserControl>().item = null;
							pickableItem.owner = null;
						}
					}
				}
			}
		}
	}

	public void Fire()
	{
		fireBullet = true;
	}
}











