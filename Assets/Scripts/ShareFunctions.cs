using UnityEngine;
using System.Collections;
using UnitySampleAssets.Characters.ThirdPerson;

public class ShareFunctions : MonoBehaviour {

	public static void PickUpItem(GameObject owner, GameObject item)
	{
		ItemID .ItemType id = item.GetComponent<ItemID>().itemType;
		AudioSource itemSound;

		switch (id) {
		case ItemID.ItemType.Health:

			CharacterStats cs = owner.GetComponent<CharacterStats>();
			cs.heal(25);
			//deshabbilito la esfera de colision
			item.GetComponentInChildren<SphereCollider>().enabled = false;
			//desasignar el dueño del PickableItem
			item.GetComponentInChildren<PickableItem>().owner.GetComponent<ThirdPersonUserControl>().canPickUp = false;
			item.GetComponentInChildren<PickableItem>().owner.GetComponent<ThirdPersonUserControl>().item = null;
			item.GetComponentInChildren<PickableItem>().owner = null;
			Destroy(item);
			break;

		case ItemID.ItemType.Weapons:
			WeaponManager wm = owner.GetComponent<WeaponManager>();

			if(wm.WeaponList.Count < wm.maxCarryingWeapons){
				wm.WeaponList.Add(item);
			}
			else
			{
				wm.ActiveWeapon.equip = false;
				wm.ActiveWeapon.hasOwner = false;
				wm.ActiveWeapon.transform.parent = null;

				GameObject removeWeapon = null;

				foreach(GameObject go in wm.WeaponList){
					if(go == wm.ActiveWeapon.gameObject)
					{

						removeWeapon = go;
					}
				}
				wm.WeaponList.Remove(removeWeapon);
				wm.WeaponList.Add(item);
			}

			item.transform.parent = owner.transform;
			item.GetComponent<WeaponControler>().hasOwner = true;

			break;

		case ItemID.ItemType.Ammo:
			item.GetComponentInChildren<PickableItem>().owner.GetComponent<WeaponManager>().ActiveWeapon.currentCarryingAmmo += 30;
			item.GetComponentInChildren<SphereCollider>().enabled = false;
			//desasignar el dueño del PickableItem
			item.GetComponentInChildren<PickableItem>().owner.GetComponent<ThirdPersonUserControl>().canPickUp = false;
			item.GetComponentInChildren<PickableItem>().owner.GetComponent<ThirdPersonUserControl>().item = null;
			item.GetComponentInChildren<PickableItem>().owner = null;
			Destroy(item);
			break;


			break;

		case ItemID.ItemType.Wearable:
			break;
		}
	}

	public static bool CheckAmmo(WeaponControler AW)
	{
		if (AW.curAmmo > 0) {
			return true;
		}
		else 
		{
			return false;
		}
	}




}
