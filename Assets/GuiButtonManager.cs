﻿using UnityEngine;
using System.Collections;

public class GuiButtonManager : MonoBehaviour {

	public void OnPushRestart(){
		Application.LoadLevel (Application.loadedLevel);
	}
	public void OnPushExit(){
		Application.Quit ();
	}
	public void OnPushStart(){
		Application.LoadLevel ("EscenaPrototipo");
	}
	public void OnPushBackTomain(){
		Application.LoadLevel (0);
	}
}
